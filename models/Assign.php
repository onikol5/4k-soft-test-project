<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assign".
 *
 * @property integer $id
 * @property integer $posts_id
 * @property integer $mark
 * @property string $data
 */
class Assign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posts_id', 'mark', 'data'], 'required'],
            [['posts_id', 'mark'], 'integer'],
            [['data'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'posts_id' => 'Posts ID',
            'mark' => 'Mark',
            'data' => 'Data',
        ];
    }
}
