﻿<?
use app\assets\AppAsset;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $content string */
/* @var $this \yii\web\View */
AppAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<?= Html::csrfMetaTags() ?>
	<meta charset="<?= Yii::$app->charset ?>">
	<? $this->registerMetaTag(['name'=>'viewport', 'content'=>'width=device-width, initial-scale=1']);?>
	<title><?= Yii::$app->name ?></title>
	<? $this->head() ?>
</head>
<body>
<? $this->beginBody(); ?>
	<div class="wrap">
	<?
	NavBar::begin(
		[
			'options' => [
				'class' => 'navbar',
				'id' => 'main-menu'
			],
			'brandLabel' => 'Тестовое приложение',
			'brandUrl' => ['/main/index']
		]
	);
		
	$menuItems = [
	];	
	
	if (Yii::$app->user->isGuest):
		$menuItems[] = [
			'label' => 'Регистрация',
			'url' => ['/main/reg']
			];
		$menuItems[] = [
				'label' => 'Войти',
				'url' => ['/main/login']
			];
	else:
		$menuItems[] = [
			'label' => 'Выйти ('.Yii::$app->user->identity['username'].')',
			'url' => ['/main/logout'],
			'linkOptions' => ['data-method' => 'post']
			];
	endif;
		
	echo Nav::widget([
		'items' => $menuItems,
		'options' => [
			'class' => 'navbar-nav navbar-right'
		]
	]);
	
	ActiveForm::begin([
		'action' => ['/main/search'],
		'method' => 'post',
		'options' => ['class' => 'navbar-form navbar-right']
	]);
	echo '<div class="input-group input-group-sm">';
	echo Html::input(
		'type:text',
		'search',
		'',
		[
			'placeholder' => 'Найти...',
			'class' => 'form-control'
		]
	);
	echo '<span class="input-group-btn">';
	echo Html::SubmitButton(
		'<span class="glyphicon glyphicon-search"></span>',
		[
			'class' => 'btn btn-success'
		]
	);
	echo "</span></div>";
	ActiveForm::end();
	
	NavBar::end();
	?>
		<div class="container">
			<?= $content ?>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<span class="badge">
				<span class="glyphicon glyphicon-copyright-mark"></span> Oleg <?= date("Y") ?>
			</span>
		</div>
	</footer>
<? $this->endBody(); ?>
</body>
</html>
<?
$this->endPage();
?>