﻿<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
	/* @var $search string */
?>
<div class="posts-index">

    <h1>Результат поиска по <?= $title ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
	
	if (count($posts) > 0)
	{?>
	
    <div class="row">
		<?
		foreach ($posts as $arr)
		{
			$name = "";
			$search = (new Query())
			->select(['*'])
			->from('user')
			->where(['id' => $arr['user_id']])
			->all();
			foreach($search as $arr1)
				$name = $arr1['username'];
			?>
				<div class="col-sm-6 col-md-4">
					<div class="thumbnail">
						<p> <? echo $name." - ".$arr['text']." - ".$arr['date']; ?> </p>
						<?if (!Yii::$app->user->isGuest):?>
							<p><a href="main/update/<?=$arr['id']?>" class="btn btn-primary" role="button">Редактировать</a>  <a href="main/delete/<?=$arr['id']?>" class="btn btn-primary" role="button" data-confirm="Вы уверены, что хотите удалить запись" >Удалить</a></p>
						<?endif;?>
					</div>
				</div>
			<?			
			//}
		}
		?>
	</div>
	<?
	}
	else{
	?>
		<div class="alert alert-danger" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Error:</span>
		  В системе нету записей
		</div>
	<?	
	}
	?>
</div>
<?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);

?>