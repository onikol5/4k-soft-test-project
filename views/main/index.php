<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;

error_reporting(E_ALL);
ini_set("display_errors", 1);

/* @var $this yii\web\View */

//$this->registerJsFile('@web/js/main-index.js',['position' => $this::POS_HEAD],'main-index');
//$this->registerJs('alert("Hello World!!!")', $this::POS_READY, 'hello-message');
//$this->registerCssFile('@web/css/main.css');
//$this->registerCss("body{background: #ff0;}");
?>
<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<? 
	$someText = [];
	
	if (!Yii::$app->user->isGuest):
		$someText[] = ['class' => 'yii\grid\ActionColumn'];
	?>
    <p>
        <?= Html::a('Добавить слово', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('Назначение значений', ['assigning'], ['class' => 'btn btn-success']) ?>
    </p>
	<?endif;
	if (count($posts) > 0)
	{?>
    <div class="row">
		<?		
			foreach ($posts as $arr)
			{

				$name = "";
				$search = (new Query())
				->select(['*'])
				->from('user')
				->where(['id' => $arr['user_id']])
				->all();
				foreach($search as $arr1)
					$name = $arr1['username'];
			?>
				<div class="col-sm-6 col-md-4">
					<div class="thumbnail">
						<p> <? echo $name." - ".$arr['text']." - ".$arr['date']." <a href='main/stat/".$arr['id']."'>Статистика</a>"; ?> </p>
						<?if (!Yii::$app->user->isGuest):?>
							<p><a href="main/update/<?=$arr['id']?>" class="btn btn-primary" role="button">Редактировать</a>  <a href="main/delete/<?=$arr['id']?>" class="btn btn-primary" role="button" data-confirm="Вы уверены, что хотите удалить запись" >Удалить</a></p>
						<?endif;?>
					</div>
				</div>
			<?			
				echo "</p>";
			}		
		?>
	</div>
	<?
	}
	else{
	?>
		<div class="alert alert-danger" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Error:</span>
		  В системе нету записей
		</div>
	<?	
	}
	?>

</div>
<?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);

?>
